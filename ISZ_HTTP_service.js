var http = require("http");
const {execFile} = require('child_process');
var fs = require('fs');
var qs = require('querystring');

http.createServer(function(request, response) {
    var body = '';
    request.on('data', function(data) {
        body += data;

        // Too much POST data, kill the connection!
        // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
        if (body.length > 1e6)
            request.connection.destroy();
    });

    request.on('end', function() {
        var post = qs.parse(body);
        fs.writeFile("Gsat_1.in", body, function(err) {
            if (err) {
                return console.log(err);
            }

            console.log("The file was saved!");
        });
        response.setHeader('Access-Control-Allow-Origin', '*');
        response.setHeader('Access-Control-Request-Method', '*');
        response.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET');
        response.setHeader('Access-Control-Allow-Headers', '*');
        response.writeHead(200, {
            'Content-Type': 'text/plain'
        });
        execFile('./b.out', (error, stdout, stderr) => {
            if (error) {
                response.end(`exec error: ${error}`);
                return;
            }
            fs.readFile('EPH.OUT', function(err, data) {
                response.write(data);
                response.end();
            });
        });
    });




}).listen(8081);

// Console will print the message
console.log('Server running at http://127.0.0.1:8081/');